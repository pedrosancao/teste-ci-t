<?php

namespace Tests;

use PHPUnit\Framework\TestCase;

class FileExtensionTest extends TestCase
{
    public function testFileExtension()
    {
        $this->assertSame('mp4', getFileExtension('music.mp4'));
        $this->assertSame('mov', getFileExtension('video.mov'));
        $this->assertSame('jpeg', getFileExtension('imagem.jpeg'));
    }
}