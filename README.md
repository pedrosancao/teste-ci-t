# Teste CI&T PHP
## Considerações

- Escolhi PHP 8 para experimentar com os novos recursos da versão (_named arguments_ e _constructor property promotion_).
- Utilizei docker para facilitar a execução e evitar erros de "funciona na minha máquina".
- Fiquei na dúvida quanto ao padrão de idioma das funções, pois os exercícios sugerem nomes em português e em inglês.
  Por fim decidir usar padrão em inglês e usar português somente onde solicitado.
- Escolhi o composer para estrutura de _autoloading_ e _scripts_, posteriormente utilizado para incluir os pacotes de testes.
- Utilizei testes unitários somente para o exercício de extensão de arquivos.
- Procurei usar mensagens de _commit_ resumidas e informativas usando [essas 7 regras][regras-commit].
- Segui ao máximo o PSR-1, exceto no exercício de 2 devido à sua simplicidade. 

## Dependências

- Docker
- Composer
## Executando

Clone esse repositório e acesse o diretório.

Instale os pacotes do composer e inicie o serviço:

```bash
composer install
composer start
```

Acesse http://localhost:9000 para visualizar.

## Executando testes

Use o script do composer:

```bash
composer test
```

[regras-commit]: https://gist.github.com/pedrosancao/10e40e27fc3a8a8ee836f0eea57ab8f7#recomenda%C3%A7%C3%B5es-para-uma-boa-mensagem-1
