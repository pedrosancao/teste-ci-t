<?php

/**
 * Sort locations by capital
 * 
 * @param $location array of contries with capitals
 * @return void
 */
function sortByCapitals(array &$location) : void
{
    usort($location, function ($a, $b) {
        return $a['capital'] <=> $b['capital'];
    });
}
