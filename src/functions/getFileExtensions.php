<?php

/**
 * Get file extension from file
 *
 * @param string $file filename
 * @return string file extension
 */
function getFileExtension(string $file) : string
{
    return pathinfo($file, PATHINFO_EXTENSION);
}