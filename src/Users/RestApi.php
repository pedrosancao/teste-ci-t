<?php

namespace PedroSancao\Users;

use PedroSancao\TxtDatabase;

class RestApi
{
    private string $databaseFile = 'users.txt';
    private TxtDatabase $database;

    /**
     * Create new instance.
     */
    public function __construct()
    {
        $this->database = new TxtDatabase($this->databaseFile);
    }

    /**
     * Persist data when instace is destroyed.
     */
    public function __destruct()
    {
        $this->database->save();
    }

    /**
     * Restrict request methods.
     *
     * @param array $allowedMethods
     * @return void
     */
    public function restrictRequestMethod(array $allowedMethods) : void
    {
        $uppercaseAllowedMethods = array_map('strtoupper', $allowedMethods);
        $requestMethod = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        if (!in_array($requestMethod, $uppercaseAllowedMethods)) {
            $this->sendResponse(['error' => 'Method not allowed.'], 405);
            exit;
        }
    }

    /**
     * Parse and return JSON body. May exit the script with bad JSON.
     *
     * @return void
     */
    public function parseRequestBody() : array
    {
        $requestBody = json_decode(file_get_contents('php://input'), true);
        if (null === $requestBody) {
            $this->sendResponse(['error' => 'Bad JSON.'], 405);
            exit;
        }

        return $requestBody;
    }

    /**
     * Send JSON response.
     * 
     * @param array $data
     * @param int $status
     * @return void
     */
    public function sendResponse(array $data = [], int $status = 200) : void
    {
        header('Content-Type: application/json');
        http_response_code($status);
        if (count($data) > 0) {
            echo json_encode($data);
        }
    }

    /**
     * Possibilita leitura dos dados atuais da instância de forma centralizada
     * dentro da classe, independente de qual parte do código estiver acessando.
     *
     * @param callable $callback função para executar leitura dos dados
     * @return void
     */
    public function readFromDatabase(callable $callback) : void
    {
        $this->database->read($callback);
    }

    /**
     * Possibilita leitura e atualização de dados em uma única chamada. Permitindo
     * manipulação dos dados em memória e de forma centralizada dentro da classe,
     * independente de qual parte do código estiver acessando.
     *
     * @param callable $callback função para executar leitura e atualização dos dados
     * @return void
     */
    public function updateDatabase(callable $callback) : void
    {
        $this->database->update($callback);
    }
}
