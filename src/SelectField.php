<?php

namespace PedroSancao;

class SelectField
{
    /**
     * Create new instance.
     *
     * @param array $options
     */
    public function __construct(
        private string $name,
        private array $options,
        private null|string $label = null,
        private string $placeholder = 'select an option'
    ) {}

    public function toHtml()
    {
        $htmlFormat = '<select id="%1$s" name="%1$s">%3$s</select>';
        if (!empty($this->label)) {
            $htmlFormat = '<label for="%1$s">%2$s</label>' . $htmlFormat;
        }
        $options = ["'<option value=\"\">{$this->placeholder}</option>"];
        foreach ($this->options as $value => $option) {
            $options[] = "<option value=\"{$value}\">{$option}</option>";
        }
        $options = join("\n", $options);

        return sprintf($htmlFormat, $this->name, $this->label, $options);
    }
}