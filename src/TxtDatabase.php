<?php

namespace PedroSancao;

class TxtDatabase
{
    protected array $data = [];

    /**
     * Cria nova instância.
     */
    public function __construct(
        protected string $file = 'data.txt'
    ) {
        $this->loadData();
    }

    /**
     * Retorna caminho do arquivo de dados.
     *
     * @return string
     */
    protected function getFilePath() : string
    {
        return dirname(filter_input(INPUT_SERVER, 'DOCUMENT_ROOT')) . '/data/' . $this->file;
    }

    /**
     * Lê conteúdo do arquivo de dados.
     *
     * @return void
     */
    protected function loadData() : void
    {
        $file = $this->getFilePath();
        if (file_exists($file)) {
            $this->data = require $file;
        }
    }

    /**
     * Possibilita leitura dos dados atuais da instância de forma centralizada
     * dentro da classe, independente de qual parte do código estiver acessando.
     *
     * @param callable $callback função para executar leitura dos dados
     * @return void
     */
    public function read(callable $callback) : void
    {
        $callback($this->data);
    }

    /**
     * Possibilita leitura e atualização de dados em uma única chamada. Permitindo
     * manipulação dos dados em memória e de forma centralizada dentro da classe,
     * independente de qual parte do código estiver acessando.
     *
     * @param callable $callback função para executar leitura e atualização dos dados
     * @return void
     */
    public function update(callable $callback) : void
    {
        $data = $callback($this->data);
        $this->data = is_array($data) ? $data : [];
    }

    /**
     * Persiste os dados da instância no arquivo.
     * 
     * @return bool
     */
    public function save() : bool
    {
        $content = '<?php return ' . var_export($this->data, true) . ';';
        return false !== file_put_contents($this->getFilePath(), $content);
    }
}