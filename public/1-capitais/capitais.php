<?php

require '../../vendor/autoload.php';

$location = [[
    'contry' => 'Japão',
    'capital' => 'Tóquio',
],[
    'contry' => 'Coreia do Sul',
    'capital' => 'Seul',
],[
    'contry' => 'México',
    'capital' => 'Cidade do México',
],[
    'contry' => 'Rússia',
    'capital' => 'Moscou',
],[
    'contry' => 'Irã',
    'capital' => 'Teerã',
],[
    'contry' => 'Indonésia',
    'capital' => 'Jacarta',
],[
    'contry' => 'Peru',
    'capital' => 'Lima',
],[
    'contry' => 'China República Popular da China',
    'capital' => 'Pequim',
],[
    'contry' => 'Egito',
    'capital' => 'Cairo',
],[
    'contry' => 'Reino Unido',
    'capital' => 'Londres',
],[
    'contry' => 'Brasil',
    'capital' => 'Brasília',
],[
    'contry' => 'EUA',
    'capital' => 'Washington',
],[
    'contry' => 'Espanha',
    'capital' => 'Madri',
]];

sortByCapitals($location);

array_map(function ($item) {
    echo "A capital do <b>{$item['contry']}</b> é <b>{$item['capital']}</b>.<br>\n";
}, $location);
