<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Gravar dados em arquivo TXT</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
<?php
    $message = filter_input(INPUT_GET, 'message');
    if ($message) {
        echo "<h2>{$message}.</h2>\n";
    }
    $errors = filter_input(INPUT_GET, 'errors', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    if ($errors) { ?>
        <h2>Erros:</h2>
        <?php
        foreach ($errors as $error) {
            echo "<p>{$error}.</p>\n";
        }
    }
?>
    <form action="salvar.php" method="POST">
        <label for="name">Nome</label>
        <input type="text" id="name" name="name" placeholder="Nome">
        <br>
        <label for="surname">Sobrenome</label>
        <input type="text" id="surname" name="surname" placeholder="Sobrenome">
        <br>
        <label for="email">E-mail</label>
        <input type="email" id="email" name="email" placeholder="E-mail">
        <br>
        <label for="phone">Telefone</label>
        <input type="text" id="phone" name="phone" placeholder="(99) 99999-9999">
        <br>
        <label for="login">Login</label>
        <input type="text" id="login" name="login" placeholder="Login">
        <br>
        <label for="password">Senha</label>
        <input type="password" id="password" name="password">
        <br>
        <br>
        <button type="submit">Gravar</button>
    </form>
</body>
</html>