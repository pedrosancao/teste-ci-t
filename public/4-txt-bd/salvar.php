<?php

require '../../vendor/autoload.php';

if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') !== 'POST') {
    header('Location: ./');
    exit;
}

$formData = filter_input_array(INPUT_POST, [
    'name' => FILTER_DEFAULT,
    'surname' => FILTER_DEFAULT,
    'email' => FILTER_VALIDATE_EMAIL,
    'phone' => FILTER_DEFAULT,
    'login' => FILTER_DEFAULT,
    'password' => FILTER_DEFAULT,
]);
$errors = [];
$errorEmail = $errorLogin = false;
if (empty($formData['name'])) {
    $errors[] = 'Nome vazio';
}
if (empty($formData['surname'])) {
    $errors[] = 'Sobrenome vazio';
}
if (empty($formData['email'])) {
    $errorEmail = true;
    $errors[] = 'E-mail vazio ou inválido';
}
if (empty($formData['phone'])) {
    $errors[] = 'Telefone vazio';
} elseif (!preg_match('#\(\d{2}\) \d{5}-\d{4}#', $formData['phone'])) {
    $errors[] = 'Telefone inválido';
}
if (empty($formData['login'])) {
    $errorLogin = true;
    $errors[] = 'Login vazio';
}
if (empty($formData['password'])) {
    $errors[] = 'Senha vazia';
}

$registros = new PedroSancao\TxtDatabase('registros.txt');
if (!$errorEmail && !$errorLogin) {
    $registros->read(function ($data) use ($formData, &$errors, &$errorEmail, &$errorLogin) {
        foreach ($data as $record) {
            if (!$errorEmail && $record['email'] === $formData['email']) {
                $errors[] = 'E-mail em uso';
                if ($errorLogin) {
                    return;
                }
                $errorEmail = true;
            }
            if (!$errorLogin && $record['login'] === $formData['login']) {
                $errors[] = 'Login em uso';
                if ($errorEmail) {
                    return;
                }
                $errorLogin = true;
            }
        }
    });
}

if (count($errors) > 0) {
    header('Location: ./?' . http_build_query(['errors' => $errors]));
    exit;
}

$registros->update(function ($data) use ($formData) {
    $formData['password'] = sha1($formData['password']);
    $data[] = $formData;
    return $data;
});
$registros->save();
header('Location: ./?' . http_build_query(['message' => 'Registro salvo']));
