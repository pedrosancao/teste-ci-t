<?php

$xml = simplexml_load_file('./cd_catalog.xml');
$rootNode = $xml->getName();
$rowProperty = $xml->children()->getName();

$csvHeaders = [];
foreach ($xml->{$rowProperty}[0] as $property) {
    $csvHeaders[] = $property->getName();
}

header('Content-Type: text/csv');
header("Content-Disposition: attachment; filename={$rootNode}.csv;");
$standardOutput = fopen('php://output', 'w');
fputcsv($standardOutput, $csvHeaders);

foreach ($xml->{$rowProperty} as $entry) {
    $row = [];
    foreach ($entry->children() as $v) {
        $row[] = (string) $v;
    }
    fputcsv($standardOutput, $row);
}

fclose($standardOutput);
