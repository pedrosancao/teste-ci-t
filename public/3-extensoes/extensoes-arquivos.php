<?php

require '../../vendor/autoload.php';

$arquivos = ['music.mp4', 'video.mov', 'imagem.jpeg'];
foreach ($arquivos as $arquivo) {
    $extensao = getFileExtension($arquivo);
    echo "A extensão do arquivo <b>{$arquivo}</b> é <b>{$extensao}</b><br>\n";
}
