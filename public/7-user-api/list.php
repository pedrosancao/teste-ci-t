<?php

require '../../vendor/autoload.php';

$api = new PedroSancao\Users\RestApi;
$api->restrictRequestMethod(['get']);
$api->readFromDatabase(function ($data) use ($api) {
    $api->sendResponse(array_values($data));
});
