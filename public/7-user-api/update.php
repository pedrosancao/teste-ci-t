<?php

require '../../vendor/autoload.php';

$api = new PedroSancao\Users\RestApi;
$api->restrictRequestMethod(['put']);
$email = filter_input(INPUT_GET, 'email', FILTER_VALIDATE_EMAIL);
if (empty($email)) {
    $errors = ['E-mail vazio ou inválido'];
    $api->sendResponse(compact('errors'), 400);
    exit;
}
$api->readFromDatabase(function ($data) use ($api, $email) {
    if (!key_exists($email, $data)) {
        $api->sendResponse(status: 404);
        exit;
    }
});

$requestBody = $api->parseRequestBody();
$requestData = filter_var_array($requestBody, [
    'name' => FILTER_DEFAULT,
    'surname' => FILTER_DEFAULT,
    'email' => FILTER_VALIDATE_EMAIL,
    'phone' => FILTER_DEFAULT,
]);
$errors = [];
if (false === $requestData['email']) {
    $errors[] = 'E-mail inválido';
} elseif ($requestData['email'] && $requestData['email'] !== $email) {
    $api->readFromDatabase(function ($data) use ($requestData, &$errors) {
        if (key_exists($requestData['email'], $data)) {
            $errors[] = 'E-mail em uso';
        }
    });
}
if ($requestData['phone'] && !preg_match('#\(\d{2}\) \d{5}-\d{4}#', $requestData['phone'])) {
    $errors[] = 'Telefone inválido';
}
if (count($errors) > 0) {
    $api->sendResponse(compact('errors'), 400);
    exit;
}

$api->updateDatabase(function ($data) use ($email, $requestData) {
    // update index
    if ($requestData['email'] && $requestData['email'] !== $email) {
        $data[$requestData['email']] = $data[$email];
        unset($data[$email]);
        $email = $requestData['email'];
    }
    foreach (array_filter($requestData) as $property => $value) {
        $data[$email][$property] = $value;
    }
    return $data;
});
$api->sendResponse(status: 204);
