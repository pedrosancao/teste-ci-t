<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>API de usuários</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <h1>API de cadastro de usuários.</h1>
    <p>
        Essa API está documentada de acordo com a
        <a href="https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html" target="_blank" rel="noopener noreferrer">RFC 2616</a>.
    </p>
    <h2>Rotas</h2>
    <p>Na pasta docs do projeto há um arquivo .rest para cada rota contendo uma requisição de exemplo.</p>
    <p>
        Para realizar as requisições sugiro instalar a extensão
        <a href="https://marketplace.visualstudio.com/items?itemName=humao.rest-client" target="_blank" rel="noopener noreferrer">REST Client</a>
        para o Visual Studio Code.
    </p>
    <hr>
    <h3>Criar usuário</h3>
    <pre><?php echo file_get_contents('../../docs/user-create.rest') ?></pre>
    <h4>Respostas esperadas</h4>
    <p>Respostas de sucesso:</p>
    <pre>HTTP/1.1 201 Created
Content-Type: application/json</pre>
    <p>Caso algum dado seja inválido:</p>
    <pre>HTTP/1.1 400 Bad Request
Content-Type: application/json
{
  "errors": [
    "Telefone inválido",
    "E-mail em uso"
  ]
}</pre>
    <hr>
    <h3>Listar usuários</h3>
    <p>
        <a href="./list.php" target="_blank">Ver a lista de usuários</a>
    </p>
    <pre><?php echo file_get_contents('../../docs/user-list.rest') ?></pre>
    <h4>Respostas esperadas</h4>
    <p>Respostas de sucesso:</p>
    <pre>HTTP/1.1 200 OK
Content-Type: application/json
[
  {
    "name": "John",
    "surname": "Doe",
    "email": "johndoe@example.com",
    "phone": "(11) 99999-9999"
  }
]</pre>
    <hr>
    <h3>Atualizar usuário</h3>
    <pre><?php echo file_get_contents('../../docs/user-update.rest') ?></pre>
    <h4>Respostas esperadas</h4>
    <p>Respostas de sucesso:</p>
    <pre>HTTP/1.1 204 No Content
Content-Type: application/json</pre>
    <p>Se o e-mail não foi cadastrado:</p>
    <pre>HTTP/1.1 404 Not Found
Content-Type: application/json</pre>
    <p>Caso algum dado seja inválido:</p>
    <pre>HTTP/1.1 400 Bad Request
Content-Type: application/json
{
  "errors": [
    "Telefone inválido",
    "E-mail em uso"
  ]
}</pre>
    <hr>
    <h3>Excluir usuário</h3>
    <pre><?php echo file_get_contents('../../docs/user-delete.rest') ?></pre>
    <h4>Respostas esperadas</h4>
    <p>Respostas de sucesso:</p>
    <pre>HTTP/1.1 204 No Content
Content-Type: application/json</pre>
    <p>Se o e-mail não foi cadastrado:</p>
    <pre>HTTP/1.1 404 Not Found
Content-Type: application/json</pre>
</body>
</html>