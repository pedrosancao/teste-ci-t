<?php

require '../../vendor/autoload.php';

$api = new PedroSancao\Users\RestApi;
$api->restrictRequestMethod(['delete']);
$email = filter_input(INPUT_GET, 'email', FILTER_VALIDATE_EMAIL);
if (empty($email)) {
    $errors = ['E-mail vazio ou inválido'];
    $api->sendResponse(compact('errors'), 400);
    exit;
}

$api->updateDatabase(function ($data) use ($api, $email) {
    $status = 404;
    if (key_exists($email, $data)) {
        unset($data[$email]);
        $status = 200;
    }
    $api->sendResponse(status: $status);
    return $data;
});
