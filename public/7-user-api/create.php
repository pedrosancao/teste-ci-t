<?php

require '../../vendor/autoload.php';

$api = new PedroSancao\Users\RestApi;
$api->restrictRequestMethod(['post']);
$requestBody = $api->parseRequestBody();
$requestData = filter_var_array($requestBody, [
    'name' => FILTER_DEFAULT,
    'surname' => FILTER_DEFAULT,
    'email' => FILTER_VALIDATE_EMAIL,
    'phone' => FILTER_DEFAULT,
]);
$errors = [];
$errorEmail = false;
if (empty($requestData['name'])) {
    $errors[] = 'Nome vazio';
}
if (empty($requestData['surname'])) {
    $errors[] = 'Sobrenome vazio';
}
if (empty($requestData['email'])) {
    $errorEmail = true;
    $errors[] = 'E-mail vazio ou inválido';
}
if (empty($requestData['phone'])) {
    $errors[] = 'Telefone vazio';
} elseif (!preg_match('#\(\d{2}\) \d{5}-\d{4}#', $requestData['phone'])) {
    $errors[] = 'Telefone inválido';
}
if (!$errorEmail) {
    $api->readFromDatabase(function ($data) use ($requestData, &$errors) {
        if (key_exists($requestData['email'], $data)) {
            $errors[] = 'E-mail em uso';
        }
    });
}
if (count($errors) > 0) {
    $api->sendResponse(compact('errors'), 400);
    exit;
}

$api->updateDatabase(function ($data) use ($requestData) {
    $data[$requestData['email']] = $requestData;
    return $data;
});
$api->sendResponse(status: 201);
