<?php

function foiMordido()
{
    return (bool) rand(0, 1);
}

printf('Joãozinho%1$s mordeu o seu dedo!', foiMordido() ? '' : ' NÃO');
?>
<br><br>
<small>Atualize a página para gerar um novo resultado.</small>