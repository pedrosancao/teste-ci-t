<?php

use PedroSancao\SelectField;

require '../../vendor/autoload.php';

$emailProviderSelect = new SelectField(
    name: 'email-provider',
    options: [
        'gmail' => '@gmail.com',
        'hotmail' => '@hotmail.com',
        'example' => '@example.com',
        'yahoo' => '@yahoo.com',
        'uol' => '@uol.com.br',
        'ig' => '@ig.com.br',
    ],
    placeholder: 'Selecione um provedor de e-mail'
);

$newsletterSelect = new SelectField(
    name: 'newsletter',
    options: [
        '1' => 'Sim',
        '0' => 'Não',
    ],
    label: 'Aceita receber nossa <i>newsletter</i>',
    placeholder: 'Selecione'
);
?><!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Formulário de exemplo</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <form method="POST">
        <label for="email">Email</label>
        <input type="text" id="email" name="email" placeholder="email">@<?php echo $emailProviderSelect->toHtml(); ?>
        <br>
        <label for="password">Senha</label>
        <input type="password" id="password" name="password">
        <br>
        <?php echo $newsletterSelect->toHtml(); ?>
        <br>
        <button type="submit">Gravar</button>
    </form>
</body>
</html>
